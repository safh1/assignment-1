import static org.junit.Assert.*;


import org.junit.Test;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class GameEngineTest {
	
	
	private Logger logger = Logger.getRootLogger();

	GameEngine eng;
	
	
	@Test
	public void playerProcessSelectAttack1() {
		GameEngine eng=new GameEngine(1,1);
		eng.processCommand("select atter dfer swing 3 duck 1");
		assertEquals("dfer swing 3 duck 1", eng.getPlayer("atter").getLatestMove().toString());
		
		String userDir = System.getProperty("user.dir");
		String configFile = String.format("%s\\%s\\%s.properties",userDir, "properties","log4j");
		PropertyConfigurator.configure(configFile);
		
		logger.info("FGds");

		//fail("Not yet implemented");
		
	}
	
	
	@Test
	public void playerProcessSelectAttack2(){
		
		/////////2 player test//////////
		eng=new GameEngine(2,2);
		eng.processCommand("select joe fred thrust 1 dodge 3");
		eng.processCommand("select fred joe swing 2 duck 2");
		assertEquals("fred thrust 1 dodge 3", eng.getPlayer("joe").getLatestMove().toString());
		assertEquals("joe swing 2 duck 2", eng.getPlayer("fred").getLatestMove().toString());
		
		eng.processUncountedCommand("roll joe",1);
		
		assertEquals("joe swings with a fast thrust and hits fred",eng.getPlayer("joe").getAttackOutcome());
		
	}
	
	@Test
	public void attackTimeTest(){
		eng=new GameEngine(2,3);
		
		eng.processCommand("select joe fred thrust 1 dodge 3");
		eng.processCommand("select fred joe swing 2 duck 2");
		eng.processUncountedCommand("roll joe",1);
		assertEquals("joe swings with a fast thrust and hits fred",eng.getPlayer("joe").getAttackOutcome());
		
		
		eng.processCommand("select joe fred thrust 2 dodge 2");
		eng.processUncountedCommand("roll joe",1);
		assertEquals("joe swings with a thrust and misses fred defending with a duck",eng.getPlayer("joe").getAttackOutcome());
		
		eng.processCommand("select joe fred thrust 3 dodge 1");
		eng.processUncountedCommand("roll joe",1);
		assertEquals("joe swings with a thrust and misses fred defending with a duck",eng.getPlayer("joe").getAttackOutcome());
	}
	
	@Test
	public void testToFindActualDirection(){
		eng=new GameEngine(2,9);
		String gen1="joe swings with a fast ";
		String gen2=" and hits fred";
		
		eng.processCommand("select joe fred thrust 1 dodge 3");
		eng.processCommand("select fred joe swing 2 duck 2");
		//tests with a base thrust 
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"thrust"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"smash"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"swing"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		
		//tests with a base swing
		eng.processCommand("select joe fred swing 1 dodge 3");
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"swing"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"thrust"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"smash"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		
		
		//test with  a base smash
		eng.processCommand("select joe fred smash 1 dodge 3");
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"smash"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"swing"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"thrust"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
	}
	
	@Test 
	public void testAllCombinationsOfAttackAndDefend(){
		eng=new GameEngine(2,9);
		String gen1="joe swings with a ";
		String gen2=" fred defending with a duck";
		
		eng.processCommand("select joe fred thrust 2 dodge 2");
		
		
		
		//tests defence with duck
		eng.processCommand("select fred joe swing 2 duck 2");
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"thrust and misses"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"smash and hits"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"swing and misses"+gen2,eng.getPlayer("joe").getAttackOutcome());
		//testds defence with dodge
		eng.processCommand("select fred joe swing 2 dodge 2");
		gen2=" fred defending with a dodge";
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"thrust and misses"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"smash and misses"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"swing and hits"+gen2,eng.getPlayer("joe").getAttackOutcome());
		//tests defence with charge
		eng.processCommand("select fred joe swing 2 charge 2");
		gen2=" fred defending with a charge";
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"thrust and hits"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"smash and misses"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"swing and misses"+gen2,eng.getPlayer("joe").getAttackOutcome());
		
		
		
		
	}
	
	@Test
	public void testDoubleHit(){
		eng=new GameEngine(2,3);
		String gen1="joe swings with a fast ";
		String gen2=" and hits fred";
		
		eng.processCommand("select joe fred thrust 1 dodge 3");
		eng.processCommand("select fred joe swing 2 duck 2");
		eng.processUncountedCommand("roll joe",1);
		assertEquals(gen1+"thrust"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",3);
		assertEquals(gen1+"smash"+gen2,eng.getPlayer("joe").getAttackOutcome());
		eng.processUncountedCommand("roll joe",5);
		assertEquals(gen1+"swing"+gen2,eng.getPlayer("joe").getAttackOutcome());
	}
	
	
	@Test
	public void test2PlayerResolutions(){
		eng=new GameEngine(2,3);
		
		
		eng.processCommand("select joe fred thrust 1 dodge 3");
		eng.processCommand("select fred joe swing 2 duck 2");
		
		eng.processCommand("roll joe",1);
		eng.processCommand("roll fred",1);
		
		assertEquals("joe wounds: 1\nfred wounds: 1\n" ,eng.getOutcome());
		
		
		
		eng.processCommand("select joe fred smash 2 charge 2");
		eng.processCommand("select fred joe thrust 3 dodge 1");
		eng.processCommand("roll joe",3);
		eng.processCommand("roll fred",6);
		
		assertEquals("joe wounds: 1\nfred wounds: 2\n" ,eng.getOutcome());
		
		
		
		
		eng.processCommand("select joe fred swing 2 dodge 2");
		eng.processCommand("select fred joe swing 1 duck 3");
		
		eng.processCommand("roll joe",4);
		eng.processCommand("roll fred",3);
		
		assertEquals("joe wounds: 2\nfred wounds: 3\n" ,eng.getOutcome());
		
		
	}
	@Test
	public void test3Players(){
		
	}
	

}
