import java.util.HashMap;
import java.util.Map;

public class GameEngine {

	private int players, rounds,attackCount;
	private String OUTCOME="";
	public String toReturn="";
	public String toOutput;
	private Map<String,Player> thePlayers;
	
	
	public GameEngine(int players, int rounds){
		this.players=players;
		this.rounds=rounds;
		attackCount=0;
		//theMoves=new moveObj[players];
		thePlayers=new HashMap<String,Player>();
		
	}
	
	public String getOutcome(){
		return OUTCOME;
		
	}
	
	public String realAttacks(){
		toOutput="";
		thePlayers.forEach((k,v)->{
			toOutput=toOutput+v.getAttackOutcome()+"\n";
		});
		return toOutput;
	}
	
	public Player getPlayer(String toGet){
		return thePlayers.get(toGet);
	}
	
	public void processCommand(String theCommands){
		String[] toProcess=theCommands.split("\\s+");
		if(toProcess[0].equals("select")){
			moveObj theMove=new moveObj(toProcess);
			if (thePlayers.containsKey(toProcess[1])==false) {
				thePlayers.put(toProcess[1], new Player(rounds));
				thePlayers.get(toProcess[1]).playerName=toProcess[1];
			}
			thePlayers.get(toProcess[1]).addMove(theMove);
		}
		return ;
	}
	
	
	
	public void processUncountedCommand(String theCommands,int theRoll){//for the test purposes only
		String[] toProcess=theCommands.split("\\s+");
		if (toProcess[0].equals("roll")){
			thePlayers.get(toProcess[1]).getLatestMove().setRoll(theRoll);
			thePlayers.get(toProcess[1]).recordOutcome(commenceAttack(toProcess[1]));
			
		}
		
	}
	
	
	
	
	public void processCommand(String theCommands,int theRoll){  ////with pre rolled
		String[] toProcess=theCommands.split("\\s+");
		if (toProcess[0].equals("roll")){
			thePlayers.get(toProcess[1]).getLatestMove().setRoll(theRoll);
			
			attackCount++;
			if(attackCount==players){
				thePlayers.get(toProcess[1]).recordOutcome(commenceAttack(toProcess[1]));
				OUTCOME="";
				thePlayers.forEach((k,v)->{
					OUTCOME=OUTCOME+k+" wounds: "+v.wounds+"\n";
				});
				attackCount=0;
				return;
			}
			thePlayers.get(toProcess[1]).recordOutcome(commenceAttack(toProcess[1]));
		}
	}
	
	
	
	
	
	public String commenceAttack(String playerAttacking){
		return doAttack(thePlayers.get(thePlayers.get(playerAttacking).getLatestMove().getTarget()),thePlayers.get(playerAttacking));
		//return "";
		
	}
	public String doAttack(Player defender,Player attacker){/////////////might use a map as a move lookup rather than ifs
		String realAttack="";
		
		
		
		if(attacker.getLatestMove().attack.equals("thrust")){
			if (attacker.getLatestMove().roll==1||attacker.getLatestMove().roll==2){
				realAttack="thrust";
				
			}
			else if (attacker.getLatestMove().roll==3||attacker.getLatestMove().roll==4){
				realAttack="smash";
			}
			else realAttack="swing";
			
		}
		else if(attacker.getLatestMove().attack.equals("swing")){
			if (attacker.getLatestMove().roll==1||attacker.getLatestMove().roll==2){
				realAttack="swing";
			}
			else if (attacker.getLatestMove().roll==3||attacker.getLatestMove().roll==4){
				realAttack="thrust";
			}
			else realAttack="smash";
			
		}
		else if(attacker.getLatestMove().attack.equals("smash")){
			if (attacker.getLatestMove().roll==1||attacker.getLatestMove().roll==2){
				realAttack="smash";
			}
			else if (attacker.getLatestMove().roll==3||attacker.getLatestMove().roll==4){
				realAttack="swing";
			}
			else realAttack="thrust";
		}
		
		attacker.getLatestMove().modifiedAttack=realAttack;
		
		
		
		if (attacker.getLatestMove().attackSpeed<defender.getLatestMove().defenseSpeed) {
			defender.wounds++;
			return attacker.playerName+" swings with a fast "+attacker.getLatestMove().modifiedAttack+" and hits "+defender.playerName;
		}
		
		
		
		if (attacker.getLatestMove().modifiedAttack.equals("thrust") && defender.getLatestMove().defense.equals("charge")) {
			defender.wounds++;
			return attacker.playerName+" swings with a "+attacker.getLatestMove().modifiedAttack+" and hits "+defender.playerName+" defending with a "+defender.getLatestMove().defense;
		}
		if (attacker.getLatestMove().modifiedAttack.equals("swing") && defender.getLatestMove().defense.equals("dodge")) {
			defender.wounds++;
			return attacker.playerName+" swings with a "+attacker.getLatestMove().modifiedAttack+" and hits "+defender.playerName+" defending with a "+defender.getLatestMove().defense;
		}
		if (attacker.getLatestMove().modifiedAttack.equals("smash") && defender.getLatestMove().defense.equals("duck")) {
			defender.wounds++;
			return attacker.playerName+" swings with a "+attacker.getLatestMove().modifiedAttack+" and hits "+defender.playerName+" defending with a "+defender.getLatestMove().defense;
		}
		return attacker.playerName+" swings with a "+attacker.getLatestMove().modifiedAttack+" and misses "+defender.playerName+" defending with a "+defender.getLatestMove().defense;
		
	}
	
}
