
public class Player {
	
	public int score, playerNumber, moveCount,wounds;
	public String playerName="";
	public moveObj[] moves;
	public String [] attackOutcome;
	
	
	public Player(int rounds){
		moves= new moveObj[rounds];
		attackOutcome=new String[rounds];
		moveCount=0;
	}
	public void addMove(moveObj theMove){
		moves[moveCount] = theMove;
		moveCount++;
	}
	public moveObj getLatestMove(){
		return moves[moveCount-1];
	}
	public String getPlayerName(){
		return playerName;
	}
	public void recordOutcome(String theOutcome){
		attackOutcome[moveCount-1]=theOutcome;
	}
	public String getAttackOutcome(){
		return attackOutcome[moveCount-1];
	}
	
}
