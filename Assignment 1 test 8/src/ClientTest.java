import static org.junit.Assert.*;
import org.junit.Test;
public class ClientTest {

	public Client toTest,toTest2;
	public Server theServer;
	@Test
	public void test() {
		theServer=new Server();
		toTest= new Client();
		assertEquals(true,toTest.connected);
		theServer=null;
		toTest=null;
	}
	
	@Test
	public void multipleClientTest(){
		Server theServer=new Server(12346);
		
		Client toTest=new Client(12346);
		Client toTest2=new Client(12346);
		
		assertEquals(true,toTest.connected);
		assertEquals(true,toTest2.connected);
		
		Client toTest3=new Client();
		assertEquals(true,toTest3.connected);
		toTest3=null;
		toTest2=null;
		toTest=null;
		theServer=null;
		
		
		
	}
	
	
	
	@Test
	public void twoClientReceive(){
		Server theServer=new Server(12320);
		Client toTest=new Client(12320);
		Client toTest2=new Client(12320);

		theServer.sendToAllTest("testing");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testing",toTest.messageIn);
		assertEquals("testing",toTest2.messageIn);
		theServer=null;
		toTest=null;
		toTest2=null;

	}
	@Test
	public void threeClientReceive(){
		Server theServer=new Server(12322);
		Client toTest=new Client(12322);
		Client toTest2=new Client(12322);
		Client toTest3=new Client(12322);

		theServer.sendToAllTest("testing two");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testing two",toTest.messageIn);
		assertEquals("testing two",toTest2.messageIn);
		assertEquals("testing two",toTest3.messageIn);
		theServer=null;
		toTest=null;
		toTest2=null;
		toTest3=null;

	}
	@Test
	public void fourClientReceive(){
		Server theServer=new Server(12325);
		Client toTest=new Client(12325);
		Client toTest2=new Client(12325);
		Client toTest3=new Client(12325);
		Client toTest4=new Client(12325);

		theServer.sendToAllTest("testing four");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("testing four",toTest.messageIn);
		assertEquals("testing four",toTest2.messageIn);
		assertEquals("testing four",toTest3.messageIn);
		assertEquals("testing four",toTest4.messageIn);

	}
	

}
	
