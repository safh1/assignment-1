import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable {

	public Socket socket;
	public DataInputStream inStream;
	public DataOutputStream outStream;
	public String messageIn="";
	public Boolean firstMessage=false;
	
	public Boolean connected=false;
	
	public ClientThread client=null;
	
	public Thread thread=null;
	
	
	public Client(){
		try{
			socket= new Socket ("localhost", 12345);
			connected=true;
			start();
			
		}catch(Exception e){System.out.println("here");}
	}
	public Client(int port){
		try{
			socket= new Socket ("localhost", port);
			connected=true;
			start();
			
		}catch(Exception e){System.out.println("here");}
	}
	public void connect(String ipAddress, int port){
		try {
			start();		
		}catch (Exception e){}
		
	}
	
	public void start() throws Exception{
		inStream= new DataInputStream(System.in);
		outStream=new DataOutputStream(socket.getOutputStream());
			if(thread==null){
				client=new ClientThread(socket,this);
				client.start();
				thread=new Thread(this);
				thread.start();
				}

	}
	
	public void run(){
		
		while (thread!=null){
			try{
				outStream.writeUTF(inStream.readLine());
				outStream.flush();
			}catch(Exception e){}
		}
	}
	
	public void runClient(){
		
		
	}
	public void stop(){
		if (thread!=null){
			thread.stop();
			thread=null;
		}
	}
	
	public void handle(String a){
		firstMessage=true;
		messageIn=a;
		System.out.println(a);
	}

}

