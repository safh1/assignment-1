import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Random;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class Server implements Runnable {
	
	public Socket socket;
	public ServerSocket server;
	
	public Boolean gameStarted=false;
	public GameEngine theGame;
	
	public ServerThread[] serverThreads;
	public Thread thread=null;
	public static Logger logger = Logger.getRootLogger();
	
	public int numPlayers=0;
	public int tally=0;
	
	
	
	public Server(){

		numPlayers=0;
		String userDir = System.getProperty("user.dir");
		String configFile = String.format("%s\\%s\\%s.properties",userDir, "properties","log4j");
		PropertyConfigurator.configure(configFile);
		
		
		serverThreads=new ServerThread[4];
		try {
			server=new ServerSocket(12345);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
		
	}
	public Server(int port){

		numPlayers=0;
		String userDir = System.getProperty("user.dir");
		String configFile = String.format("%s\\%s\\%s.properties",userDir, "properties","log4j");
		PropertyConfigurator.configure(configFile);
		
		
		serverThreads=new ServerThread[4];
		try {
			server=new ServerSocket(port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
		
	}
	

	
	public void start(){
		if (thread==null){
			thread=new Thread(this);
			thread.start();
			
		}
	}
	
	public void stop(){
		if (thread!=null){
			thread.stop();
			thread=null;
		}
	}
	   public void run()
	   {  
		   
		   while (thread != null)
	      {  try
	         {  System.out.println("wait for client"); 
	            addThread(server.accept()); }
	         catch(Exception ioe)
	         {  System.out.println("Server accept error: " + ioe); }
	      }
	   }
	   
	   
	   private void addThread(Socket socket)
	   {  //if (clientCount < clients.length)
		   
		   
	        System.out.println("Client accepted: " + socket);
	         serverThreads[numPlayers] = new ServerThread(socket, this);
	         try
	         {  //serverThreads[x].open(); 
	         serverThreads[numPlayers].start();  
	            numPlayers++;
	            }
	         
	         catch(Exception ioe)
	         {  System.out.println("Error opening thread: " + ioe); } 
	      
	   }
	
	
	public void sendToGame(String sendToGame){
		
	}
	
	
	public synchronized void sendToAllTest(String sendToAll){
		for (int a=0;a<numPlayers;a++){
			serverThreads[a].sendToClient(sendToAll);
		}
	}
	public synchronized void  sendToAll(String sendToAll){
		
		System.out.println("Sending "+sendToAll);
		//serverThreads[0].sendToClient(sendToAll);
		for (int a=0;a<numPlayers;a++){
			serverThreads[a].sendToClient(sendToAll);
		}
		String[] temp=sendToAll.split("\\s+");
		if (temp[0].equals("startGame")) {
			logger.info(" RECEIVED "+sendToAll+" "+(new Date()).toGMTString());
			startGame(temp);
			}
		
		if (temp[0].equals("roll")) {
			logger.info(" RECEIVED "+sendToAll+" "+(new Date()).toGMTString());
			tally++;
			makeRoll(sendToAll);
			}
		if(temp[0].equals("select"))  {
			logger.info(" RECEIVED "+sendToAll+" "+(new Date()).toGMTString());
			theGame.processCommand(sendToAll);
		}
		if (tally==numPlayers){
			logger.info(" SEND "+theGame.getOutcome()+" "+(new Date()).toGMTString());
			tally=0; 
			sendToAll(theGame.realAttacks());
			logger.info(" SEND "+theGame.realAttacks());
			sendToAll(theGame.getOutcome());
			}
	}
	
	public void makeRoll(String  temp){
		Random rand= new Random();
		int n = rand.nextInt(5)+1;
		theGame.processCommand(temp, n);
	}
	
	public void startGame(String[] temp){
		gameStarted=true;
		theGame=new GameEngine(Integer.parseInt(temp[1]),Integer.parseInt(temp[2]));
		
		}
}
