public class moveObj {
	
	public String target,attack,defense,modifiedAttack;
	public int attackSpeed,defenseSpeed, roll;
	
	public moveObj(String[] info){
		target=info[2];
		attack=info[3];
		attackSpeed=Integer.parseInt(info[4]);
		defense=info[5];
		defenseSpeed=Integer.parseInt(info[6]);
	}
	public String toString(){
		return target+" "+attack+" "+attackSpeed+" "+defense+" "+defenseSpeed;
	}
	public void setRoll(int theRoll){
		roll=theRoll;
	}

	public String getTarget(){
		return target;
	}
}
