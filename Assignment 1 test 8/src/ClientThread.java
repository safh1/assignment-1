import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientThread extends Thread{

	
	public Socket socket;
	public DataInputStream inStream;
	public Client theClient;
	
	public ClientThread(Socket theSocket,Client theClient){
		
		this.theClient=theClient;
		socket=theSocket;
		try {
			inStream=new DataInputStream(socket.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void run(){
		while (true){
			try {
				//String in=inStream.readUTF();
				theClient.handle(inStream.readUTF());
				//theClient.recMessage(in);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("problem here");
			}
		}
		
	}

}
